package com.themohitrao.core;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.themohitrao.common.models.venue.Venue;
import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.sdk.datasource.DaggerComponentDataSource;
import com.themohitrao.sdk.network.ModuleNetworkData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DetailViewModel extends BaseViewModel {

    private MutableLiveData<Venue> mVenueDetail;
    private String mSearchText;
    private VenueSnapShot mSnapShot;
    private MutableLiveData<Boolean> mCollapsedState;

    DetailViewModel(Application application) {
        super(application);
        initialize();
    }

    private void initialize() {
        mVenueDetail = new MutableLiveData<>();
        mCollapsedState = new MutableLiveData<>();
        mCollapsedState.setValue(false);
    }

    private void onSuccessVenue(Venue venueFullDetailDetail) {
        mLoaderVisibility.setValue(false);
        if (venueFullDetailDetail == null) {
            mVenueDetail.setValue(null);
            setSubTitleMessage(String.format(getApplication()
                    .getResources().getString(R.string.no_results_for), mSearchText));
        } else {
            venueFullDetailDetail.setCategories(mSnapShot.getCategories());
            venueFullDetailDetail.setLocation(mSnapShot.getLocation());
            setSubTitleMessage(String.format(getApplication()
                    .getResources().getString(R.string.showing_results_for), mSearchText));
            mVenueDetail.setValue(venueFullDetailDetail);
        }
    }

    public void initiateDetailsRequest(String venueId) {
        mCompositeDisposable.add(getDisposable(venueId));
    }

    public MutableLiveData<Venue> getVenueDetail() {
        return mVenueDetail;
    }

    /**
     * Method to return Disposable instance for an rx call.
     *
     * @param venueId id of the venue whose details are needed
     * @return instance of Disposable
     */
    private Disposable getDisposable(String venueId) {
        return DaggerComponentDataSource.builder()
                .moduleNetworkData(new ModuleNetworkData("", "", venueId))
                .build()
                .getVenuesDetail()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::onSuccessVenue, this::onError);
    }

    public void setExistingData(VenueSnapShot snapShot) {
        this.mSnapShot = snapShot;
    }

    public void setCollapsedState(boolean isCollapsed) {
        this.mCollapsedState.setValue(isCollapsed);
    }

    public MutableLiveData<Boolean> getCollapsedState() {
        return mCollapsedState;
    }
}