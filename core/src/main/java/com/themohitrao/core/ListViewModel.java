package com.themohitrao.core;

import android.app.Application;
import androidx.lifecycle.MutableLiveData;

import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.sdk.datasource.ComponentDataSource;
import com.themohitrao.sdk.datasource.DaggerComponentDataSource;
import com.themohitrao.sdk.network.ModuleNetworkData;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ListViewModel extends BaseViewModel {

    private MutableLiveData<List<VenueSnapShot>> mVenueList;
    private MutableLiveData<List<String>> mSearchList;
    private String mSearchText;

    ListViewModel(Application application) {
        super(application);
        initialize();
    }

    private void initialize() {
        mVenueList = new MutableLiveData<>();
        mSearchList = new MutableLiveData<>();
        mCompositeDisposable.add(getDisposable());
    }

    void onSuccessVenueList(List<VenueSnapShot> Models) {
        mLoaderVisibility.setValue(false);
        if (Models == null || Models.isEmpty()) {
            mVenueList.setValue(new ArrayList<>());
            setSubTitleMessage(String.format(getApplication()
                    .getResources().getString(R.string.no_results_for), mSearchText));
        } else {
            setSubTitleMessage(String.format(getApplication()
                    .getResources().getString(R.string.showing_results_for), mSearchText));
            mVenueList.setValue(Models);
        }
    }

    public MutableLiveData<List<VenueSnapShot>> getVenueList() {
        return mVenueList;
    }

    /**
     * Method to initiate request for fetching data.
     */
    public void initiateRequest(String categoryId, String query) {
        mLoaderVisibility.setValue(true);
        Disposable disposable = getDisposable(categoryId, query);
        if (disposable != null) {
            mCompositeDisposable.add(disposable);
        }
    }

    /**
     * Method to return Disposable instance for an rx call.
     *
     * @param categoryId category id which needs to be searched
     * @param query search text against which venues are needed
     * @return instance of Disposable
     */
    private Disposable getDisposable(String categoryId, String query) {
        ComponentDataSource component = DaggerComponentDataSource.builder()
                .moduleNetworkData(new ModuleNetworkData(categoryId, query,""))
                .build();
        if (!categoryId.isEmpty()) {
            mSearchText = categoryId;
            setSubTitleMessage(String.format(getApplication()
                    .getResources().getString(R.string.showing_results_for), mSearchText));
            return component.getVenuesByCategory()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::onSuccessVenueList, this::onError);
        } else if (!query.isEmpty()) {
            mSearchText = query;
            setSubTitleMessage(String.format(getApplication()
                    .getResources().getString(R.string.showing_results_for), mSearchText));
            return component.getVenuesByQuery()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::onSuccessVenueList, this::onError);
        }
        return null;
    }

    private void onSuccessSearchList(List<String> newList) {
        mLoaderVisibility.setValue(false);
        if (newList == null || newList.isEmpty()) {
            mSearchList.setValue(new ArrayList<>());
        } else {
            mSearchList.setValue(newList);
        }
    }

    public MutableLiveData<List<String>> getSearchList() {
        return mSearchList;
    }

    private Disposable getDisposable() {
        return DaggerComponentDataSource.builder()
                .moduleNetworkData(new ModuleNetworkData("", "",""))
                .build()
                .getSearchList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessSearchList, this::onError);
    }

    public void clearList() {
        this.mVenueList.setValue(new ArrayList<>());
        setSubTitleMessage("");
    }
}