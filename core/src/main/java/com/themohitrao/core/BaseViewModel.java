package com.themohitrao.core;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel extends AndroidViewModel {

    MutableLiveData<Boolean> mLoaderVisibility;
    private MutableLiveData<Boolean> mSubTitleVisibility;
    CompositeDisposable mCompositeDisposable;
    private MutableLiveData<String> mSubTitleMessage;

    BaseViewModel(Application application) {
        super(application);
        initializeComponents();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (mCompositeDisposable != null) {
            mCompositeDisposable.dispose();
            mCompositeDisposable = null;
        }
    }

    private void initializeComponents() {
        mLoaderVisibility = new MutableLiveData<>();
        mSubTitleVisibility = new MutableLiveData<>();
        mCompositeDisposable = new CompositeDisposable();
        mSubTitleMessage = new MutableLiveData<>();
        mSubTitleVisibility.setValue(true);
    }

    void onError(Throwable throwable) {
        setSubTitleMessage(throwable.getMessage());
        mLoaderVisibility.setValue(false);
    }

    public MutableLiveData<Boolean> getLoaderVisibility() {
        return mLoaderVisibility;
    }

    public MutableLiveData<String> getSubTitleMessage() {
        return mSubTitleMessage;
    }

    void setSubTitleMessage(String message) {
        this.mSubTitleMessage.setValue(message);
    }

    MutableLiveData<Boolean> getSubTitleVisibility() {
        return mSubTitleVisibility;
    }

    public void setSubTitleVisibility(boolean subTitleVisibility) {
        this.mSubTitleVisibility.setValue(subTitleVisibility);
    }
}