package com.themohitrao.core;

import android.app.Application;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.themohitrao.common.models.venue.VenueSnapShot;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class ListViewModelTest {

    private static final String DUMMY_STRING = "dummyString";
    @ClassRule
    public static final InstantTaskExecutorRule rule = new InstantTaskExecutorRule();
    @ClassRule
    public static final RxImmediateSchedulerRule newRule = new RxImmediateSchedulerRule();

    private ListViewModel mListViewModel;

    @Before
    public void setUp() {
        Application application = Mockito.mock(Application.class);
        mListViewModel = new ListViewModel(application);
    }

    private List<VenueSnapShot> getDummyList(boolean isEmpty) {
        if (isEmpty) {
            return new ArrayList<>();
        } else {
            List<VenueSnapShot> list = new ArrayList<>();
            list.add(new VenueSnapShot());
            return list;
        }
    }

    @Test
    public void dataNotNullOnInitialization() {
        assertNotNull(mListViewModel.getErrorMessage());
        assertNotNull(mListViewModel.getSubTitleMessage());
        assertNotNull(mListViewModel.getSubTitleVisibility());
        assertNotNull(mListViewModel.getVenueList());
        assertNotNull(mListViewModel.getLoaderVisibility());
    }

    @Test
    public void getLoadingVisibilityOnRequestInitiation() {
        mListViewModel.initiateRequest(DUMMY_STRING, DUMMY_STRING);
        assertTrue(mListViewModel.getLoaderVisibility().getValue());
    }

    @Test
    public void getLoadingVisibilityOnSuccessFullResponseButEmptyList() {
        mListViewModel.onSuccessVenueList(getDummyList(true));
        assertFalse(mListViewModel.getLoaderVisibility().getValue());
    }

    @Test
    public void getLoadingVisibilityOnSuccessFullResponseButNonEmptyList() {
        mListViewModel.onSuccessVenueList(getDummyList(false));
        assertFalse(mListViewModel.getLoaderVisibility().getValue());
    }

    @Test
    public void getLoadingVisibilityOnError() {
        mListViewModel.onError(new Throwable());
        assertFalse(mListViewModel.getLoaderVisibility().getValue());
    }

}