package com.themohitrao.myapplication.activities;

import com.themohitrao.myapplication.R;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class MainActivityTest {


    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class);


    @Test
    public void testProgressVisible() {
        onView(withId(R.id.progress)).check(matches(isDisplayed()));
    }

    @Test
    public void testSubtitleVisible() {
        onView(withId(R.id.text_view_sub_title)).check(matches(isDisplayed()));
    }

    @After
    public void tearDown() {
        activityRule.finishActivity();
    }

}