package com.themohitrao.myapplication.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.themohitrao.myapplication.contracts.DetailInteractor;
import com.themohitrao.myapplication.databinding.FragmentDetailBinding;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailInteractor} interface
 * to handle interaction events.
 */
public class DetailFragment extends Fragment {

    private DetailInteractor mListener;

    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentDetailBinding binding = FragmentDetailBinding.inflate(inflater);
        binding.setClickListener(mListener);
        binding.setViewModel(mListener.getDetailViewModel());
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof DetailInteractor) {
            mListener = (DetailInteractor) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DetailInteractor");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
