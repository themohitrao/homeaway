package com.themohitrao.myapplication.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.themohitrao.common.models.venue.Venue;
import com.themohitrao.myapplication.R;
import com.themohitrao.myapplication.contracts.DetailInteractor;
import com.themohitrao.myapplication.databinding.FragmentMapBinding;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailInteractor} interface
 * to handle interaction events.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, GoogleMap.OnMyLocationButtonClickListener {

    private DetailInteractor mInteractor;
    private GoogleMap mMap;
    private LatLngBounds mBounds;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof DetailInteractor) {
            mInteractor = (DetailInteractor) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DetailInteractor");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentMapBinding binding = FragmentMapBinding.inflate(inflater);
        binding.setViewModel(mInteractor.getDetailViewModel());
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpMap(view);
    }

    private void setUpMap(View view) {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mInteractor = null;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setPadding(50, 50, 50, 80);
        mMap.setOnMapLoadedCallback(this);
        mMap.setOnMyLocationButtonClickListener(this);
    }

    private void setLocationOnMap(GoogleMap map, Venue venueFullDetail) {
        if (map == null) {
            return;
        }
        if (venueFullDetail == null) {
            return;
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(Double.valueOf("47.6062"), Double.valueOf("-122.3321")));
        builder.include(new LatLng(venueFullDetail.getLocation().getLat(), venueFullDetail.getLocation().getLng()));
        mBounds = builder.build();

        MarkerOptions destination = new MarkerOptions();
        destination.position(new LatLng(venueFullDetail.getLocation().getLat(), venueFullDetail.getLocation().getLng()));
        destination.title(venueFullDetail.getName());
        destination.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        map.addMarker(destination);

        MarkerOptions source = new MarkerOptions();
        source.position(new LatLng(Double.valueOf("47.6062"), Double.valueOf("-122.3321")));
        source.title("Seattle, WA");
        source.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        map.addMarker(source);

        map.setLatLngBoundsForCameraTarget(mBounds);
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(mBounds, 200));

    }

    @Override
    public void onMapLoaded() {
        addObservers();
    }

    private void addObservers() {
        mInteractor.getDetailViewModel().getVenueDetail().observe(this, this::OnVenueRefreshed);
    }

    private void OnVenueRefreshed(Venue venue) {
        setLocationOnMap(mMap, venue);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if (mMap != null && mBounds != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(mBounds, 200));
        }
        return true;
    }
}
