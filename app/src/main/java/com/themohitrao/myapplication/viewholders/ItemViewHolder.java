package com.themohitrao.myapplication.viewholders;

import androidx.recyclerview.widget.RecyclerView;

import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.myapplication.databinding.LayoutItemBinding;

/**
 * View Holder class for the object of {@link VenueSnapShot}
 */
public class ItemViewHolder extends RecyclerView.ViewHolder {
    private final LayoutItemBinding mBinding;

    public ItemViewHolder(LayoutItemBinding mBinding) {
        super(mBinding.getRoot());
        this.mBinding = mBinding;
    }

    /**
     * Method to bind view
     *
     * @param item model object
     */
    public void bind(VenueSnapShot item) {
        mBinding.setVenueSnapShot(item);
        mBinding.executePendingBindings();
    }

    /**
     * Method to unbind view
     */
    public void unbind() {
        if (mBinding != null) {
            mBinding.unbind();
        }
    }

    public LayoutItemBinding getBinding() {
        return mBinding;
    }
}