package com.themohitrao.myapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.themohitrao.common.models.GenericListWrapper;
import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.core.ListViewModel;
import com.themohitrao.core.ViewModelFactory;
import com.themohitrao.myapplication.R;
import com.themohitrao.myapplication.adapters.RecyclerViewAdapterVenueList;
import com.themohitrao.myapplication.contracts.ListItemInteractor;
import com.themohitrao.myapplication.databinding.ActivityMainBinding;
import com.themohitrao.myapplication.util.Utility;

/**
 * Main Class to populate and navigate details for the top repository list of git
 */
public class MainActivity extends AppCompatActivity implements ListItemInteractor, SearchView.OnQueryTextListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setUpBinding();
    }

    /**
     * Method to hide the status bar to make it a full Screen Activity
     */
    protected void hideStatusBar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * Set up Binding for this activity
     */
    private void setUpBinding() {
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setAdapter(new RecyclerViewAdapterVenueList(this));
        binding.setListener(this);
        binding.setListViewModel(getViewModel());
        binding.setInteractor(this);
        binding.setLifecycleOwner(this);
    }

    @Override
    public void onSelected(VenueSnapShot selectedObjected) {
        getViewModel().setSubTitleVisibility(false);
        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
        intent.putExtra(DetailActivity.VENUE_TAG, selectedObjected);
        startActivity(intent);
    }

    @Override
    public void onMapSelected() {
        Intent intent = new Intent(getApplicationContext(),MapsActivity.class);
        intent.putExtra(MapsActivity.VENUE_LIST, new GenericListWrapper(getViewModel().getVenueList().getValue()));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        getViewModel().setSubTitleVisibility(true);
        super.onBackPressed();
    }

    @Override
    public boolean onQueryTextSubmit(String searchText) {
        Utility.hideKeyboard(this);
        getViewModel().initiateRequest("", searchText);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }


    /**
     * method getter for {@link ListViewModel}
     *
     * @return instance of the same
     */
    private ListViewModel getViewModel() {
        return ViewModelProviders.of(this,
                ViewModelFactory.getInstance(getApplication()))
                .get(ListViewModel.class);
    }

}
