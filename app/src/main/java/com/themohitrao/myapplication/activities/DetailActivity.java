package com.themohitrao.myapplication.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.AppBarLayout;
import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.core.DetailViewModel;
import com.themohitrao.core.ListViewModel;
import com.themohitrao.core.ViewModelFactory;
import com.themohitrao.myapplication.R;
import com.themohitrao.myapplication.contracts.DetailInteractor;
import com.themohitrao.myapplication.databinding.ActivityDetailBinding;

public class DetailActivity extends FragmentActivity implements DetailInteractor,
        AppBarLayout.OnOffsetChangedListener {

    public static final String VENUE_TAG = "VENUE_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extractData();
        this.getWindow().setFlags(WindowManager.
                LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ActivityDetailBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_detail);
        binding.setDetailViewModel(getViewModel());
        binding.setOffsetChangeListener(this);
        binding.setLifecycleOwner(this);
    }


    @Override
    public void onClickReadMore() {
        openLink(getDetailViewModel().getVenueDetail().getValue().getCanonicalUrl());
    }

    @Override
    public void onClickVisitWebsite() {
        openLink(getDetailViewModel().getVenueDetail().getValue().getUrl());
    }

    @Override
    public void onClickTwitter() {
        openLink(getDetailViewModel().getVenueDetail().getValue().getContact() == null ? null : "https://twitter.com/" + getDetailViewModel().getVenueDetail().getValue().getContact().getTwitter());
    }

    @Override
    public void onClickFacebook() {
        openLink(getViewModel().getVenueDetail().getValue().getContact() == null ? null : "https://www.facebook.com/" + getViewModel().getVenueDetail().getValue().getContact().getFacebook());
    }

    @Override
    public void onClickWhatsApp() {
        openLink(getDetailViewModel().getVenueDetail().getValue().getContact() == null ? null : "tel:" + getDetailViewModel().getVenueDetail().getValue().getContact().getPhone());
    }

    @Override
    public DetailViewModel getDetailViewModel() {
        return getViewModel();
    }

    private void extractData() {
        if (getIntent().hasExtra(VENUE_TAG)) {
            VenueSnapShot snapShot = getIntent().getParcelableExtra(VENUE_TAG);
            getViewModel().setExistingData(snapShot);
            getViewModel().initiateDetailsRequest(snapShot.getId());
        } else {
            Toast.makeText(this, "No Id found for this Venue", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * method getter for {@link ListViewModel}
     *
     * @return instance of the same
     */
    private DetailViewModel getViewModel() {
        return ViewModelProviders.of(this,
                ViewModelFactory.getInstance(getApplication()))
                .get(DetailViewModel.class);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
            //  Collapsed
            getViewModel().setCollapsedState(true);
        } else {
            getViewModel().setCollapsedState(false);
        }

    }

    /**
     * Method to open url in browser
     *
     * @param url string url
     */
    private void openLink(String url) {
        if (url == null || url.equals("")) {
            Toast.makeText(getApplicationContext(), "No valid url found", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "No application can handle this request."
                    + " Please install a web browser", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
