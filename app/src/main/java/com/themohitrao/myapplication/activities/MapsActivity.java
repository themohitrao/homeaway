package com.themohitrao.myapplication.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.themohitrao.common.lrucache.LRUCacheFactory;
import com.themohitrao.common.models.GenericListWrapper;
import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.myapplication.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMapLoadedCallback, GoogleMap.OnInfoWindowClickListener {

    public static final String VENUE_LIST = "VENUE_LIST";
    private GoogleMap mMap;
    private List<VenueSnapShot> mVenues = new ArrayList<>();
    private HashMap<Marker, Integer> mMarkers = new HashMap<>();
    private LatLngBounds.Builder mBoundBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLoadedCallback(this);
        mMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onMapLoaded() {
        getListFromIntent();

    }

    /**
     * Method to hide the status bar to make it a full Screen Activity
     */
    protected void hideStatusBar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void getListFromIntent() {
        if (getIntent().hasExtra(VENUE_LIST)) {
            mVenues = ((GenericListWrapper) getIntent().getParcelableExtra(VENUE_LIST)).getList();
            startLoadingData();
        }
    }

    private void startLoadingData() {
        VenueSnapShot snapShot;
        mBoundBuilder = new LatLngBounds.Builder();
        for (int position = 0; position < mVenues.size(); position++) {
            snapShot = mVenues.get(position);
            if (snapShot.getLocation() != null
                    && snapShot.getLocation().getLat() != null
                    && snapShot.getLocation().getLng() != null) {
                mMarkers.put(setMarkerInMap(snapShot), position);
            }
        }
        if (mMarkers.size() > 0) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(mBoundBuilder.build(), 200));
        }
    }

    private Marker setMarkerInMap(VenueSnapShot snapShot) {
        MarkerOptions mTempOptions = new MarkerOptions();
        mTempOptions.title(snapShot.getName());
        mTempOptions.position(
                new LatLng(snapShot.getLocation().getLat(), snapShot.getLocation().getLng()));
        mBoundBuilder.include(
                new LatLng(snapShot.getLocation().getLat(), snapShot.getLocation().getLng()));
        if (snapShot.getCategories().size() > 0
                && snapShot.getCategories().get(0).getIcon() != null) {
            mTempOptions.snippet(snapShot.getCategories().get(0).getName());
            mTempOptions.icon(getBitmap(
                    snapShot.getCategories().get(0).getIcon().getPrefix()
                            + "64"
                            + snapShot.getCategories().get(0).getIcon().getSuffix()
                    , snapShot.getName()));
        }
        return mMap.addMarker(mTempOptions);
    }

    private BitmapDescriptor getBitmap(String url, String name) {
        Bitmap bitmap = LRUCacheFactory.getInstance().getBitmap(url);
        if (bitmap != null) {
            Bitmap backgroundBitmap =
                    BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_map_icon);
            Bitmap resultBitmap = Bitmap.createBitmap(backgroundBitmap.getWidth(),
                    backgroundBitmap.getHeight(), backgroundBitmap.getConfig());
            Canvas canvas = new Canvas(resultBitmap);
            canvas.drawBitmap(backgroundBitmap, new Matrix(), null);
            canvas.drawBitmap(bitmap, Float.valueOf(String.valueOf((backgroundBitmap.getWidth() - bitmap.getWidth()) / 2)),
                    Float.valueOf(String.valueOf((backgroundBitmap.getHeight() - bitmap.getHeight()) / 2)), new Paint());

            return BitmapDescriptorFactory.fromBitmap(resultBitmap);
        } else {
            return BitmapDescriptorFactory.defaultMarker();
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if (mMarkers.get(marker) == null) {
            return;
        }
        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
        intent.putExtra(DetailActivity.VENUE_TAG, mVenues.get(mMarkers.get(marker)));
        startActivity(intent);
    }
}
