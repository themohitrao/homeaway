package com.themohitrao.myapplication.contracts;

import com.themohitrao.common.models.venue.VenueSnapShot;

public interface ListItemInteractor {

    void onSelected(VenueSnapShot selectedObjected);

    void onMapSelected();
}
