package com.themohitrao.myapplication.contracts;

import com.themohitrao.core.DetailViewModel;
import com.themohitrao.myapplication.fragments.DetailFragment;

/**
 * Contract for defining click listeners of {@link DetailFragment}
 */
public interface DetailInteractor {

    void onClickReadMore();

    void onClickVisitWebsite();

    void onClickTwitter();

    void onClickFacebook();

    void onClickWhatsApp();

    DetailViewModel getDetailViewModel();
}
