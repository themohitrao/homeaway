package com.themohitrao.myapplication.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Utility {
    /**
     * Method to hide keyPad using a specific view
     *
     * @param view    view which needs to be given focus before cancelling keyboard.
     * @param context calling activity in which keypad need to hidden
     */
    public static void hideKeyboard(View view, Context context) {
        if (context == null || view == null) {
            return;
        }
        InputMethodManager inputMethodManager
                = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputMethodManager.showSoftInput(view, 0);
        view.requestFocus();
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    /**
     * Method to hide keyPad
     *
     * @param activity calling activity in which keypad need to hidden
     */
    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager
                    = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager
                    .hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
