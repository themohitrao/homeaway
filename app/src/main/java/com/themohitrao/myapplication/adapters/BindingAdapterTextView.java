package com.themohitrao.myapplication.adapters;

import androidx.databinding.BindingAdapter;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.TextView;

import com.themohitrao.myapplication.R;

/**
 * Helper Class for defining various binding adapter methods
 */
public class BindingAdapterTextView {

    private BindingAdapterTextView() {
        //METHOD NOT IN USE
    }


    /**
     * Method to set underlined text
     *
     * @param textView target text view
     * @param text     string value to be underlined and set.
     */
    @BindingAdapter("setUnderLinedText")
    public static void setUnderline(TextView textView, String text) {
        if (text == null) {
            text = "";
        }
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
        textView.setText(content);
    }

    @BindingAdapter("distanceText")
    public static void setDistanceText(TextView textView, Integer distance) {
        textView.setText(distance == null || distance == 0 ? textView.getContext()
                .getResources().getString(R.string.info_unavailable) :
                getDistanceInString(distance));
    }

    private static String getDistanceInString(Integer distance) {
        if (distance > 1000) {
            return Math.round(Double.valueOf(String.valueOf(distance)) / 1000 * 10.0) / 10.0 + " km";
        } else {
            return distance + " mtr";
        }
    }

}
