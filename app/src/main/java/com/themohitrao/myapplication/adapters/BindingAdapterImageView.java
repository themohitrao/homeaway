package com.themohitrao.myapplication.adapters;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.BindingAdapter;

import android.graphics.Bitmap;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.themohitrao.common.lrucache.LRUCacheFactory;
import com.themohitrao.myapplication.R;

/**
 * Helper Class for defining various binding adapter methods
 */
public class BindingAdapterImageView {

    private BindingAdapterImageView() {
        //METHOD NOT IN USE
    }

    /**
     * Method to set image into an image view
     *
     * @param imgView target imageview
     * @param imgUrl  url of the imageview bitmap
     */
    @BindingAdapter("imgUrl")
    public static void setImage(ImageView imgView, String imgUrl) {
        Log.e("IMAGE_URL", imgUrl);
        Bitmap bitmap = LRUCacheFactory.getInstance().getBitmap(imgUrl);

        if (bitmap == null) {
            Glide.with(imgView.getContext())
                    .load(imgUrl)
                    .asBitmap()
                    .placeholder(R.drawable.layout_shape_round)
                    .error(R.drawable.layout_shape_round)
                    .into(new BitmapImageViewTarget(imgView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            LRUCacheFactory.getInstance().insertBitmap(imgUrl, resource);
                            imgView.setImageBitmap(resource);
                        }
                    });
        } else {
            imgView.setImageBitmap(bitmap);
        }
    }

    /**
     * Method to set round image into an image view
     *
     * @param imageView target image view
     * @param imgUrl    url of the image view bitmap
     */
    @BindingAdapter("imgRoundUrl")
    public static void setRoundImage(ImageView imageView, String imgUrl) {
        Bitmap bitmap = LRUCacheFactory.getInstance().getBitmap(imgUrl);

        if (bitmap == null) {
            Glide.with(imageView.getContext())
                    .load(imgUrl)
                    .asBitmap()
                    .centerCrop()
                    .dontAnimate()
                    .placeholder(R.drawable.layout_shape_round)
                    .error(R.drawable.layout_shape_round)
                    .into(new BitmapImageViewTarget(imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            LRUCacheFactory.getInstance().insertBitmap(imgUrl, resource);
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        } else {
            RoundedBitmapDrawable circularBitmapDrawable =
                    RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), bitmap);
            circularBitmapDrawable.setCircular(true);
            imageView.setImageDrawable(circularBitmapDrawable);
        }
    }

    /**
     * Method to set round image into an image view
     *
     * @param toolbar target toolbar
     * @param imgUrl  url of the imageview bitmap
     */
    @BindingAdapter("customCollapseIcon")
    public static void setRoundImage(Toolbar toolbar, String imgUrl) {
        Bitmap bitmap = LRUCacheFactory.getInstance().getBitmap(imgUrl);

        if (bitmap == null) {
            Glide.with(toolbar.getContext())
                    .load(imgUrl)
                    .asBitmap()
                    .placeholder(R.drawable.layout_shape_round)
                    .error(R.drawable.layout_shape_round)
                    .into(new BitmapImageViewTarget(new ImageView(toolbar.getContext())) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            LRUCacheFactory.getInstance().insertBitmap(imgUrl, resource);
                            toolbar.setLogo(new BitmapDrawable(toolbar.getContext().getResources(), resource));
                        }
                    });
        } else {
            toolbar.setLogo(new BitmapDrawable(toolbar.getContext().getResources(), bitmap));
        }
    }

}
