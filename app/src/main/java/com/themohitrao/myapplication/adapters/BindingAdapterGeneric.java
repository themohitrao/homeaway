package com.themohitrao.myapplication.adapters;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.View;

import androidx.databinding.BindingAdapter;

/**
 * Helper Class for defining various binding adapter methods
 */
public class BindingAdapterGeneric {

    private BindingAdapterGeneric() {
        //METHOD NOT IN USE
    }

    /**
     * Method to generate color on the basis of text input
     *
     * @param view       target view whose background color needs to change
     * @param inputValue text input
     */
    @BindingAdapter(value = {"colorFromText", "alpha"}, requireAll = false)
    public static void setColor(View view, String inputValue, Integer value) {
        if (inputValue != null) {
            view.setBackgroundColor(Color.parseColor(getHexCode(inputValue, value)));
        }
    }

    /**
     * Method to generate color on the basis of text input
     *
     * @param view       target view whose background color needs to change
     * @param inputValue text input
     */
    @BindingAdapter("tintColor")
    public static void setBackgroundTint(View view, String inputValue) {
        if (inputValue != null) {
            view.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(getHexCode(inputValue, 100))));
        }
    }

    private static String getHexCode(String inputValue, Integer value) {
        char[] letters = inputValue.toCharArray();
        int totalValue = 0;
        for (char ch : letters) {
            totalValue = totalValue + (int) ch;
        }
        return "#" + (value == null ? "" : Integer.toHexString(getCorrectTransparencyRange(value))) + Integer.toHexString(getCorrectRange(totalValue));
    }

    private static int getCorrectTransparencyRange(Integer value) {
        return Double.valueOf(value.doubleValue() / 100 * 255).intValue();
    }

    private static int getCorrectRange(int totalValue) {
        if (totalValue / 99999 >= 1) {
            return totalValue + 9000000;
        } else if (totalValue / 9999 >= 1) {
            return totalValue + 9990000;
        } else if (totalValue / 999 >= 1) {
            return totalValue + 9999000;
        } else if (totalValue / 99 >= 1) {
            return totalValue + 9999900;
        } else if (totalValue / 9 >= 1) {
            return totalValue + 9999990;
        } else {
            return 999999;
        }

    }

}
