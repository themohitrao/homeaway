package com.themohitrao.myapplication.adapters;

import androidx.databinding.BindingAdapter;

import com.google.android.material.appbar.AppBarLayout;

/**
 * Helper Class for defining various binding adapter methods
 */
public class BindingAdapterAppBarLayout {

    private BindingAdapterAppBarLayout() {
        //METHOD NOT IN USE
    }

    @BindingAdapter("offsetChangeListener")
    public static void setOffsetChangeListener(AppBarLayout appBarLayout, AppBarLayout.OnOffsetChangedListener listener) {
        if (listener != null) {
            appBarLayout.addOnOffsetChangedListener(listener);
        }
    }

}
