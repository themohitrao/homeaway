package com.themohitrao.myapplication.adapters;

import androidx.databinding.BindingAdapter;
import androidx.appcompat.widget.SearchView;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.themohitrao.myapplication.util.Utility;

import java.util.List;

/**
 * Helper Class for defining various binding adapter methods
 */
public class BindingAdapterSearchView {

    private BindingAdapterSearchView() {
        //METHOD NOT IN USE
    }


    @BindingAdapter("onQuerySearchListener")
    public static void setOnQueryTextListener(SearchView searchView, SearchView.OnQueryTextListener listener) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.onActionViewCollapsed();
                listener.onQueryTextSubmit(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                listener.onQueryTextChange(s);
                return false;
            }
        });
    }

    @BindingAdapter("textColor")
    public static void setTextColorSearchView(SearchView searchView, int colorId) {
        EditText searchEditText = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(colorId);
    }

    @BindingAdapter("hintTextColor")
    public static void setHintTextColorSearchView(SearchView searchView, int colorId) {
        EditText searchEditText = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setHintTextColor(colorId);
    }

    @BindingAdapter("searchList")
    public static void setSearchList(SearchView searchView, List<String> list) {
        if (list == null) {
            return;
        }
        String[] array = list.toArray(new String[0]);
        SearchView.SearchAutoComplete searchAutoComplete
                = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(searchView.getContext(),
                android.R.layout.simple_dropdown_item_1line, array);
        searchAutoComplete.setAdapter(dataAdapter);
        searchAutoComplete.setOnItemClickListener((parent, view, position, id) -> searchView.setQuery(dataAdapter.getItem(position), true));
        searchAutoComplete.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Utility.hideKeyboard(searchAutoComplete, searchView.getContext());
                searchView.setQuery(v.getText().toString(), true);
                searchAutoComplete.dismissDropDown();
                return true;
            }
            return false;
        });
    }

}
