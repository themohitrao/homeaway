package com.themohitrao.myapplication.adapters;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.common.util.AppCoreConstants;

import java.util.List;

/**
 * Helper Class for defining various binding adapter methods
 */
public class BindingAdapterList {

    private BindingAdapterList() {
        //METHOD NOT IN USE
    }

    /**
     * This method provides the functionality to set a list into a recycler view
     *
     * @param rclView recyclerview which needs to be set list in
     * @param data    list which is to be set
     */
    @BindingAdapter("list")
    public static void setList(RecyclerView rclView, List<VenueSnapShot> data) {
        if (data != null) {
            RecyclerViewAdapterVenueList recyclerViewAdapterVenueList = ((RecyclerViewAdapterVenueList) rclView.getAdapter());
            if (recyclerViewAdapterVenueList == null) {
                return;
            }
            recyclerViewAdapterVenueList.setList(data);
            if(rclView.getLayoutManager()==null){
                return;
            }
            rclView.getLayoutManager().scrollToPosition(AppCoreConstants.ZERO);
            recyclerViewAdapterVenueList.notifyDataSetChanged();
        }
    }

}
