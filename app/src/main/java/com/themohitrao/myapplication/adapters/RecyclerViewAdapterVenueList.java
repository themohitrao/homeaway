package com.themohitrao.myapplication.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.myapplication.contracts.ListItemInteractor;
import com.themohitrao.myapplication.databinding.LayoutItemBinding;
import com.themohitrao.myapplication.viewholders.ItemViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for list recycler view
 */
public class RecyclerViewAdapterVenueList extends RecyclerView.Adapter<ItemViewHolder> {

    private List<VenueSnapShot> mList;
    private ListItemInteractor mClickListener;

    public RecyclerViewAdapterVenueList(ListItemInteractor clickListener) {
        this.mList = new ArrayList<>();
        this.mClickListener = clickListener;
    }

    @Override
    @NonNull
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(
                LayoutItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int position) {
        if (!mList.isEmpty()) {
            itemViewHolder.bind(mList.get(position));
            itemViewHolder.getBinding().setClickListener(mClickListener);
        }
    }

    @Override
    public void onViewDetachedFromWindow(ItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<VenueSnapShot> newList) {
        this.mList.clear();
        this.mList.addAll(newList);
    }
}
