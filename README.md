# HomeAway

HomeAway Assignment Covers the following requirements :

  - The main screen should display a search input, and should use industry best practices to perform a type ahead search against the Foursquare API.
  - When a search returns results, these should be displayed in a list format. Each list item should provide, at a minimum, the name of the place (e.g., Flitch Coffee), the category of the place (e.g., Coffee Shop), the icon from the response, the distance from the center of Seattle (47.6062° N, 122.3321° W) to the place. Clicking a list item should launch the details screen for that place.
  - The details screen for a place should use a collapsible toolbar layout to show a map in the upper half of the screen, with two pins -- one, the location of search result, and the other, the center of Seattle. The bottom half of the details screen should provide details about the place.
  - The user should be able to navigate between screens according to Android platform conventions.
  - When a search returns results, the main screen should include a Floating Action Button(This has been skipped, as a button along with the search bar was implemented which executes the same job). Clicking the Floating Action Button should launch a full-screen map with a pin for every search result. Clicking a pin should show the name of the place on the map, and clicking on the name should then open the details screen for the given place.
  - Include a link to the place’s website (if it exists) on the details screen. Clicking this link should open an external Intent to a browser installed on the device. (Foursquare provides additional details, including this link, via another API)

 ---
# Architecture:

  - MVVM Clean has been followed to build the application.
  - Application consists of 4 modules : app, core, sdk and common
  - As mentioned above, MVVM clean has been implemented, hence app connects only with the core layer, and core layer connects with the sdk layer. common module has some libraries and models which are used across all the modules.
  1) app : this module basically represents the presentation layer. It has all the UI work, which would mean fragments, activities, adapters, view holders, etc
  2) core: the core layer represents the domain layer. business logic in form ViewModels have been implemented here.
  3) sdk: this layer represents the host layer. It handles the networking part. And is suppose to handle data base part as well (suppose to because i couldnt complete it in 24 hour run i made to complete this project :D )
 4) common : common layer is a utility layer, it usually handles global utilities and also sometimes acts as a bridge between modules by providing interactor contracts . hence other modules can give access to their classes via this module.

 ---
 # Libraries used:
  - Gson
  - Retrofit
  - RxJava, RxAndroid
  - Glide
  - Dagger2
  - OkHttp
  - room
  - PowerMockito
  - Mockito
  - Data Binding

 ---

# Noticeable efforts:
  My main emphasis while making this project and in real life and work life as well is that i try to look a problem in parts and solution as a whole picture. Hence, lot of my time is invested in refining my implementations so that unit problems and can be solved in the best manner possible and the solution can look CLEAN AND SIMPLE.
  From Designing (UI) to putting myself in the shoes of a user (UX) along with the thought process of writing easy to understand code and scalable solutions or approaches are the railroads of my programming.

 ---
 # You can also:
  - Check the LRU cache implementation i have done of the images in common module.
  - Check Design approach of the application: Placements of the actions and information in terms of size and color.
  - Check excessive use to data binding to minimize the getting and setting part. hence avoiding null pointer exceptions.
  - Check Documentation of the functions.

# What more i still see pending
Although the application is not even 1/5th of what it can be, but on a very basic level, here are the things i can still implement if time permits
- Realm Database
- Repository - Network Synchronization
- fine tuning the scopes of classes
- UX Interaction with search view
- First screen is blank. Some small game can be implemented there.

 ---
  # Reach Me on themohitrao@gmail.com for any kind of information or query regarding the work.

