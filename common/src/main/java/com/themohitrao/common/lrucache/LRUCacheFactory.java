package com.themohitrao.common.lrucache;

import android.graphics.Bitmap;

public class LRUCacheFactory {

    private static LRUCacheFactory mINSTANCE;

    private CustomLRUCache customLRUCache;

    private LRUCacheFactory() {
        customLRUCache = new CustomLRUCache();
    }

    public static synchronized LRUCacheFactory getInstance() {
        if (mINSTANCE == null) {
            mINSTANCE = new LRUCacheFactory();
        }
        return mINSTANCE;
    }

    public void insertBitmap(String url, Bitmap bitmap) {
        if (!customLRUCache.imageAlreadyPresentAndMovedToStart(url, bitmap)) {
            customLRUCache.insertNewImage(url, bitmap);
        }
    }


    public Bitmap getBitmap(String url) {
        if (customLRUCache.mImageHashMap.containsKey(url)) {
            Node entry = customLRUCache.mImageHashMap.get(url);
            customLRUCache.removeImage(entry);
            customLRUCache.addImage(entry);
            return entry.getBitmap();
        }
        return null;
    }
}
