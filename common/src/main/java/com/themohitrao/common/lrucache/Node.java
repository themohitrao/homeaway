package com.themohitrao.common.lrucache;

import android.graphics.Bitmap;


class Node {
        private Bitmap mBitmap;
        private String mUrl;
        private Node left;
        private Node right;

        Node(Bitmap mBitmap, String mUrl, Node left, Node right) {
            this.mBitmap = mBitmap;
            this.mUrl = mUrl;
            this.left = left;
            this.right = right;
        }


     Bitmap getBitmap() {
        return mBitmap;
    }

     void setBitmap(Bitmap mBitmap) {
        this.mBitmap = mBitmap;
    }

     String getUrl() {
        return mUrl;
    }

     void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

     Node getLeft() {
        return left;
    }

     void setLeft(Node left) {
        this.left = left;
    }

     Node getRight() {
        return right;
    }

     void setRight(Node right) {
        this.right = right;
    }
}