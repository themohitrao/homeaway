package com.themohitrao.common.lrucache;

import android.graphics.Bitmap;

import java.util.HashMap;


/**
 * This class uses a HashMap which keeps reference to a doubly Link List or two way Queue nodes.
 * the HashMap keeps the reference to the nodes on the basis of url
 * the queue keeps the cached bitmaps
 */
class CustomLRUCache {

    //byte size
    private final static int MAX_SIZE = 5 * 1024 * 1024;
    private int currentTotalSize = 0;

    private Node start;
    private Node end;

    HashMap<String, Node> mImageHashMap;

    CustomLRUCache() {
        mImageHashMap = new HashMap<>();
    }

    /**
     * method is used to add images in the queue it is added at the start of the queue.
     *
     * @param node object which contains new bitmap
     */
    void addImage(Node node) {
        currentTotalSize = currentTotalSize + node.getBitmap().getByteCount();
        node.setRight(start);
        node.setLeft(null);
        if (start != null) {
            start.setLeft(node);
        }
        start = node;
        if (end == null) {
            end = start;
        }
    }

    /**
     * method is used to remove images at the end of queue.
     *
     * @param node node which needs to be removed from this queue.
     */
    void removeImage(Node node) {
        currentTotalSize = currentTotalSize - node.getBitmap().getByteCount();
        if (node.getLeft() != null) {
            node.getLeft().setRight(node.getRight());
        } else {
            start = node.getRight();
        }

        if (node.getRight() != null) {
            node.getRight().setLeft(node.getLeft());
        } else {
            end = node.getLeft();
        }
    }

    /**
     * Method to move the an already present image to the start of the queue.
     *
     * @param url    image url
     * @param bitmap image bitmap
     * @return true if image is moved to start
     */
    boolean imageAlreadyPresentAndMovedToStart(String url, Bitmap bitmap) {
        if (mImageHashMap.containsKey(url)) {
            Node entry = mImageHashMap.get(url);
            entry.setBitmap(bitmap);
            removeImage(entry);
            addImage(entry);
            return true;
        } else {
            return false;
        }
    }

    /**
     * insert new image using this method.
     *
     * @param url    url of the image
     * @param bitmap bitmap of the image
     */
    void insertNewImage(String url, Bitmap bitmap) {
        Node newNode = new Node(bitmap, url, null, null);
        recalibrateSize(bitmap);
        addImage(newNode);
        mImageHashMap.put(url, newNode);
    }

    /**
     * this method recalibrate the queue size. if size exceeds then, removes data from the end.
     *
     * @param bitmap bitmap which is being added
     */
    private void recalibrateSize(Bitmap bitmap) {
        if (bitmap.getByteCount() + currentTotalSize > MAX_SIZE) {
            mImageHashMap.remove(end.getUrl());
            removeImage(end);
            recalibrateSize(bitmap);
        }
    }
}
