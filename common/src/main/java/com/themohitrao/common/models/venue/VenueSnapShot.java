
package com.themohitrao.common.models.venue;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VenueSnapShot implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("referralId")
    @Expose
    private String referralId;
    @SerializedName("hasPerk")
    @Expose
    private Boolean hasPerk;
    @SerializedName("venuePage")
    @Expose
    private VenuePage venuePage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getReferralId() {
        return referralId;
    }

    public void setReferralId(String referralId) {
        this.referralId = referralId;
    }

    public Boolean getHasPerk() {
        return hasPerk;
    }

    public void setHasPerk(Boolean hasPerk) {
        this.hasPerk = hasPerk;
    }

    public VenuePage getVenuePage() {
        return venuePage;
    }

    public void setVenuePage(VenuePage venuePage) {
        this.venuePage = venuePage;
    }


    public VenueSnapShot(){
        // Default Constructor so that this class can be extended
    }

    protected VenueSnapShot(Parcel in) {
        id = in.readString();
        name = in.readString();
        location = (Location) in.readValue(Location.class.getClassLoader());
        if (in.readByte() == 0x01) {
            categories = new ArrayList<>();
            in.readList(categories, Category.class.getClassLoader());
        } else {
            categories = null;
        }
        referralId = in.readString();
        byte hasPerkVal = in.readByte();
        hasPerk = hasPerkVal == 0x02 ? null : hasPerkVal != 0x00;
        venuePage = (VenuePage) in.readValue(VenuePage.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeValue(location);
        if (categories == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(categories);
        }
        dest.writeString(referralId);
        if (hasPerk == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (hasPerk ? 0x01 : 0x00));
        }
        dest.writeValue(venuePage);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<VenueSnapShot> CREATOR = new Parcelable.Creator<VenueSnapShot>() {
        @Override
        public VenueSnapShot createFromParcel(Parcel in) {
            return new VenueSnapShot(in);
        }

        @Override
        public VenueSnapShot[] newArray(int size) {
            return new VenueSnapShot[size];
        }
    };
}