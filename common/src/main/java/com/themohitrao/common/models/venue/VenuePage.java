
package com.themohitrao.common.models.venue;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VenuePage implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    protected VenuePage(Parcel in) {
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<VenuePage> CREATOR = new Parcelable.Creator<VenuePage>() {
        @Override
        public VenuePage createFromParcel(Parcel in) {
            return new VenuePage(in);
        }

        @Override
        public VenuePage[] newArray(int size) {
            return new VenuePage[size];
        }
    };
}
