package com.themohitrao.common.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.themohitrao.common.models.venue.VenueSnapShot;

import java.util.ArrayList;
import java.util.List;

public class GenericListWrapper implements Parcelable {

    private List<VenueSnapShot> mList;

    public List<VenueSnapShot> getList() {
        return mList;
    }

    public void setList(List<VenueSnapShot> mList) {
        this.mList = mList;
    }

    public GenericListWrapper(List<VenueSnapShot> list) {
        this.mList = list;

    }


    private GenericListWrapper(Parcel in) {
        if (in.readByte() == 0x01) {
            mList = new ArrayList<>();
            in.readList(mList, VenueSnapShot.class.getClassLoader());
        } else {
            mList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (mList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GenericListWrapper> CREATOR = new Parcelable.Creator<GenericListWrapper>() {
        @Override
        public GenericListWrapper createFromParcel(Parcel in) {
            return new GenericListWrapper(in);
        }

        @Override
        public GenericListWrapper[] newArray(int size) {
            return new GenericListWrapper[size];
        }
    };
}
