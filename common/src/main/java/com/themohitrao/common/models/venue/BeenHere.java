
package com.themohitrao.common.models.venue;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BeenHere {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("unconfirmedCount")
    @Expose
    private Integer unconfirmedCount;
    @SerializedName("marked")
    @Expose
    private Boolean marked;
    @SerializedName("lastCheckinExpiredAt")
    @Expose
    private Integer lastCheckInExpiredAt;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getUnconfirmedCount() {
        return unconfirmedCount;
    }

    public void setUnconfirmedCount(Integer unconfirmedCount) {
        this.unconfirmedCount = unconfirmedCount;
    }

    public Boolean getMarked() {
        return marked;
    }

    public void setMarked(Boolean marked) {
        this.marked = marked;
    }

    public Integer getLastCheckInExpiredAt() {
        return lastCheckInExpiredAt;
    }

    public void setLastCheckInExpiredAt(Integer lastCheckInExpiredAt) {
        this.lastCheckInExpiredAt = lastCheckInExpiredAt;
    }

}
