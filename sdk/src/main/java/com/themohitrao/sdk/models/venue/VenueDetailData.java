
package com.themohitrao.sdk.models.venue;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.themohitrao.common.models.venue.Venue;

public class VenueDetailData {

    @SerializedName("venue")
    @Expose
    private Venue venueFullDetail;

    public Venue getVenueFullDetail() {
        return venueFullDetail;
    }

    public void setVenueFullDetail(Venue venueFullDetail) {
        this.venueFullDetail = venueFullDetail;
    }

}
