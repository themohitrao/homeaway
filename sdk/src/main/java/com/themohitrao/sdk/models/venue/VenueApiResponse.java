
package com.themohitrao.sdk.models.venue;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.themohitrao.sdk.models.generic.Meta;

public class VenueApiResponse {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("response")
    @Expose
    private VenueApiData response;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public VenueApiData getResponse() {
        return response;
    }

    public void setResponse(VenueApiData response) {
        this.response = response;
    }

}
