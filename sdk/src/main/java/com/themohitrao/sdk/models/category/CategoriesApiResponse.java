
package com.themohitrao.sdk.models.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.themohitrao.sdk.models.generic.Meta;

public class CategoriesApiResponse {

    @SerializedName("meta")
    @Expose
    private Meta categoriesApiMeta;
    @SerializedName("response")
    @Expose
    private CategoriesApiData categoriesApiData;

    public Meta getCategoriesApiMeta() {
        return categoriesApiMeta;
    }

    public void setCategoriesApiMeta(Meta categoriesApiMeta) {
        this.categoriesApiMeta = categoriesApiMeta;
    }

    public CategoriesApiData getCategoriesApiData() {
        return categoriesApiData;
    }

    public void setCategoriesApiData(CategoriesApiData categoriesApiData) {
        this.categoriesApiData = categoriesApiData;
    }

}
