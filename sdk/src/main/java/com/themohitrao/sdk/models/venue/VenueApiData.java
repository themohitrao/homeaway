
package com.themohitrao.sdk.models.venue;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.themohitrao.common.models.venue.VenueSnapShot;

public class VenueApiData {

    @SerializedName("venues")
    @Expose
    private List<VenueSnapShot> venueSnapShots = null;
    @SerializedName("confident")
    @Expose
    private Boolean confident;

    public List<VenueSnapShot> getVenueSnapShots() {
        return venueSnapShots;
    }

    public void setVenueSnapShots(List<VenueSnapShot> venueSnapShots) {
        this.venueSnapShots = venueSnapShots;
    }

    public Boolean getConfident() {
        return confident;
    }

    public void setConfident(Boolean confident) {
        this.confident = confident;
    }

}
