
package com.themohitrao.sdk.models.venue;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.themohitrao.sdk.models.generic.Meta;

public class VenueDetailResponse {

    @SerializedName("meta")
    @Expose
    private Meta venueDetailMeta;
    @SerializedName("response")
    @Expose
    private VenueDetailData venueDetailData;

    public Meta getVenueDetailMeta() {
        return venueDetailMeta;
    }

    public void setVenueDetailMeta(Meta venueDetailMeta) {
        this.venueDetailMeta = venueDetailMeta;
    }

    public VenueDetailData getVenueDetailData() {
        return venueDetailData;
    }

    public void setVenueDetailData(VenueDetailData venueDetailData) {
        this.venueDetailData = venueDetailData;
    }

}
