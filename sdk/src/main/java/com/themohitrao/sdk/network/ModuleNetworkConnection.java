package com.themohitrao.sdk.network;

import com.themohitrao.sdk.models.category.CategoriesApiResponse;
import com.themohitrao.sdk.models.venue.VenueApiResponse;
import com.themohitrao.sdk.models.venue.VenueDetailResponse;
import com.themohitrao.sdk.scopes.NetworkScope;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Single;
import retrofit2.Retrofit;

import static com.themohitrao.sdk.network.ModuleNetworkData.NAMED_QUALIFIER_CATEGORY_ID;
import static com.themohitrao.sdk.network.ModuleNetworkData.NAMED_QUALIFIER_QUERY;
import static com.themohitrao.sdk.network.ModuleNetworkData.NAMED_QUALIFIER_VENUE_ID;

@Module(includes = {ModuleNetworkClient.class, ModuleNetworkData.class})
public class ModuleNetworkConnection {

    public static final String NAMED_QUALIFIER_CATEGORY_API = "categoriesApi";
    public static final String NAMED_QUALIFIER_VENUE_BY_CATEGORY_API = "venueByCategoryApi";
    public static final String NAMED_QUALIFIER_VENUE_BY_QUERY_API = "venueByQueryApi";
    public static final String NAMED_QUALIFIER_VENUE_DETAIL_API = "venueDetailApi";

    private static final String VERSION_DATE_FORMAT = "20190211";
    private static final String SEATTLE_MIDDLE_LOCATION = "47.61,-122.33";
    private static final String CLIENT_ID = "4EJVYIQ5EAUEXC5APMH3F5TYI2WJVACFPSXSHV5RR2LSRRBA";
    private static final String CLIENT_SECRET = "QTEKEW0GCZC1RPE3DPU3K4UY4JFN2DX3LW4ENIKLFNIAXVWA";
    private static final String RADIUS = "5000";

    @Provides
    @Named(NAMED_QUALIFIER_CATEGORY_API)
    Single<CategoriesApiResponse> getCategories(Retrofit retrofit,
                                                Class<ContractNetworkApis> service) {
        return retrofit.create(service)
                .getCategories(CLIENT_ID,
                        CLIENT_SECRET, VERSION_DATE_FORMAT);
    }

    @Provides
    @Named(NAMED_QUALIFIER_VENUE_BY_CATEGORY_API)
    Single<VenueApiResponse> getVenueByCategory(
            Retrofit retrofit,
            Class<ContractNetworkApis> service,
            @Named(NAMED_QUALIFIER_CATEGORY_ID) String categoryId) {
        return retrofit.create(service).getVenuesByCategory(categoryId,
                SEATTLE_MIDDLE_LOCATION, CLIENT_ID,
                CLIENT_SECRET, RADIUS, VERSION_DATE_FORMAT);
    }

    @Provides
    @Named(NAMED_QUALIFIER_VENUE_BY_QUERY_API)
    Single<VenueApiResponse> getVenueByQuery(
            Retrofit retrofit,
            Class<ContractNetworkApis> service,
            @Named(NAMED_QUALIFIER_QUERY) String query) {
        return retrofit.create(service).getVenuesByQuery(query,
                SEATTLE_MIDDLE_LOCATION, CLIENT_ID,
                CLIENT_SECRET, RADIUS, VERSION_DATE_FORMAT);
    }

    @Provides
    @Named(NAMED_QUALIFIER_VENUE_DETAIL_API)
    Single<VenueDetailResponse> getVenueDetails(Retrofit retrofit,
                                                Class<ContractNetworkApis> service,
                                                @Named(NAMED_QUALIFIER_VENUE_ID) String venueId) {
        return retrofit.create(service).getVenuesDetails(venueId, CLIENT_ID,
                CLIENT_SECRET, VERSION_DATE_FORMAT);
    }


    @NetworkScope
    @Provides
    Class<ContractNetworkApis> getService() {
        return ContractNetworkApis.class;
    }
}
