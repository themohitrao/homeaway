package com.themohitrao.sdk.network;

import androidx.annotation.NonNull;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ModuleNetworkData {

    static final String NAMED_QUALIFIER_CATEGORY_ID = "categoryId";
    static final String NAMED_QUALIFIER_QUERY = "searchQuery";
    static final String NAMED_QUALIFIER_VENUE_ID = "venueId";

    private String mCategoryId;
    private String mVenueId;
    private String mSearchQuery;

    public ModuleNetworkData(@NonNull String mCategoryId,
                             @NonNull String searchQuery,
                             @NonNull String venueId) {
        this.mCategoryId = mCategoryId;
        this.mSearchQuery = searchQuery;
        this.mVenueId = venueId;
    }

    @Provides
    @Named(NAMED_QUALIFIER_CATEGORY_ID)
    String getCategoryId() {
        return mCategoryId;
    }

    @Provides
    @Named(NAMED_QUALIFIER_QUERY)
    String getSearchQuery() {
        return mSearchQuery;
    }

    @Provides
    @Named(NAMED_QUALIFIER_VENUE_ID)
    String getVenueId() {
        return mVenueId;
    }
}
