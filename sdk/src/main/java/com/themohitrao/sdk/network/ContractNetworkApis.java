package com.themohitrao.sdk.network;

import com.themohitrao.sdk.models.category.CategoriesApiResponse;
import com.themohitrao.sdk.models.venue.VenueApiResponse;
import com.themohitrao.sdk.models.venue.VenueDetailResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.themohitrao.sdk.network.NetworkConstants.CATEGORY_ID;
import static com.themohitrao.sdk.network.NetworkConstants.CLIENT_ID;
import static com.themohitrao.sdk.network.NetworkConstants.CLIENT_SECRET;
import static com.themohitrao.sdk.network.NetworkConstants.LOCATION;
import static com.themohitrao.sdk.network.NetworkConstants.QUERY;
import static com.themohitrao.sdk.network.NetworkConstants.RADIUS;
import static com.themohitrao.sdk.network.NetworkConstants.VENUE_ID;
import static com.themohitrao.sdk.network.NetworkConstants.VERSION_IN_FORM_YYYY_MM_DD;

/**
 * Public contract to define GET API Call Details.
 * to be used in creating the connection call.
 */
interface ContractNetworkApis {




    /*47.6131746,-122.482147*/
    /*47.6062, -122.3321*/

    @GET("venues/search")
    Single<VenueApiResponse> getVenuesByCategory(@Query(CATEGORY_ID) String categoryId,
                                                 @Query(LOCATION) String locationValueInCommaFormat,
                                                 @Query(CLIENT_ID) String clientId,
                                                 @Query(CLIENT_SECRET) String clientSecret,
                                                 @Query(RADIUS) String radius,
                                                 @Query(VERSION_IN_FORM_YYYY_MM_DD) String versionInDateForm);

    @GET("venues/search")
    Single<VenueApiResponse> getVenuesByQuery(@Query(QUERY) String searchQuery,
                                              @Query(LOCATION) String locationValueInCommaFormat,
                                              @Query(CLIENT_ID) String clientId,
                                              @Query(CLIENT_SECRET) String clientSecret,
                                              @Query(RADIUS) String radius,
                                              @Query(VERSION_IN_FORM_YYYY_MM_DD) String versionInDateForm);

    @GET("venues/{VENUE_ID}")
    Single<VenueDetailResponse> getVenuesDetails(@Path(VENUE_ID) String venueId,
                                                 @Query(CLIENT_ID) String clientId,
                                                 @Query(CLIENT_SECRET) String clientSecret,
                                                 @Query(VERSION_IN_FORM_YYYY_MM_DD) String versionInDateForm);


    @GET("venues/categories")
    Single<CategoriesApiResponse> getCategories(@Query(CLIENT_ID) String clientId,
                                                @Query(CLIENT_SECRET) String clientSecret,
                                                @Query(VERSION_IN_FORM_YYYY_MM_DD) String versionInDateForm);


}