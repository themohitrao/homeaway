package com.themohitrao.sdk.network;

class NetworkConstants {

    private NetworkConstants() {
        //Not in use
    }

    static final String CATEGORY_ID = "categoryId";
    static final String CLIENT_ID = "client_id";
    static final String CLIENT_SECRET = "client_secret";
    static final String QUERY = "query";
    static final String VENUE_ID = "VENUE_ID";
    static final String VERSION_IN_FORM_YYYY_MM_DD = "v";
    static final String LOCATION = "ll";
    static final String RADIUS = "radius";

}
