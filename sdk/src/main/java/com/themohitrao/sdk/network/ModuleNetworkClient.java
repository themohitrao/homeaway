package com.themohitrao.sdk.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.themohitrao.sdk.scopes.NetworkScope;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class responsible for building the connection Client through which data will be fetched from server
 */
@Module
public class ModuleNetworkClient {

    private static final String BASE_URL = "https://api.foursquare.com/v2/";

    @Provides
    @NetworkScope
    Retrofit getRetrofit(OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    @NetworkScope
    OkHttpClient getOkHttpClient(HttpLoggingInterceptor interceptor) {
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    @Provides
    @NetworkScope
    HttpLoggingInterceptor getHttpLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    @NetworkScope
    Gson getGson() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }

}