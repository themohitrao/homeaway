package com.themohitrao.sdk.datasource;

import com.themohitrao.common.models.catagory.Category;
import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.common.models.venue.Venue;
import com.themohitrao.sdk.models.category.CategoriesApiResponse;
import com.themohitrao.sdk.models.venue.VenueApiResponse;
import com.themohitrao.sdk.models.venue.VenueDetailResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Single;

import static com.themohitrao.sdk.network.ModuleNetworkConnection.NAMED_QUALIFIER_CATEGORY_API;
import static com.themohitrao.sdk.network.ModuleNetworkConnection.NAMED_QUALIFIER_VENUE_BY_CATEGORY_API;
import static com.themohitrao.sdk.network.ModuleNetworkConnection.NAMED_QUALIFIER_VENUE_BY_QUERY_API;
import static com.themohitrao.sdk.network.ModuleNetworkConnection.NAMED_QUALIFIER_VENUE_DETAIL_API;

@Module
class ModuleDataSource {

    static final String NAMED_QUALIFIER_CATEGORIES = "categories";
    static final String NAMED_QUALIFIER_VENUES_BY_CATEGORY = "venueByCategory";
    static final String NAMED_QUALIFIER_VENUE_DETAIL = "venueDetail";
    static final String NAMED_QUALIFIER_VENUES_BY_QUERY = "venueByQuery";
    static final String NAMED_QUALIFIER_SEARCH_LIST = "searchKeyWordList";

    @Provides
    @Named(NAMED_QUALIFIER_CATEGORIES)
    Single<List<Category>> getCategories(
            @Named(NAMED_QUALIFIER_CATEGORY_API) Single<CategoriesApiResponse> response) {
        return response.flatMap(this::onDataFetched);
    }

    @Provides
    @Named(NAMED_QUALIFIER_VENUES_BY_CATEGORY)
    Single<List<VenueSnapShot>> getVenuesByCategory(
            @Named(NAMED_QUALIFIER_VENUE_BY_CATEGORY_API) Single<VenueApiResponse> response) {
        return response.flatMap(this::onVenuesResponse);
    }

    @Provides
    @Named(NAMED_QUALIFIER_VENUES_BY_QUERY)
    Single<List<VenueSnapShot>> getVenuesByQuery(
            @Named(NAMED_QUALIFIER_VENUE_BY_QUERY_API) Single<VenueApiResponse> response) {
        return response.flatMap(this::onVenuesResponse);
    }

    @Provides
    @Named(NAMED_QUALIFIER_SEARCH_LIST)
    Single<List<String>> getSearchList(
            @Named(NAMED_QUALIFIER_CATEGORY_API) Single<CategoriesApiResponse> response) {
        return response.flatMap(this::getStringList);
    }

    @Provides
    @Named(NAMED_QUALIFIER_VENUE_DETAIL)
    Single<Venue> getVenueDetailsById(@Named(NAMED_QUALIFIER_VENUE_DETAIL_API)
                                              Single<VenueDetailResponse> response) {
        return response.flatMap(this::getVenueDetailsFromResponse);
    }

    private Single<Venue> getVenueDetailsFromResponse(VenueDetailResponse venueDetailResponse) {
        return Single.just(venueDetailResponse.getVenueDetailData().getVenueFullDetail());
    }


    /**
     * Method converts {@link VenueApiResponse} into a list of {@link VenueSnapShot}
     *
     * @param response instance of {@link VenueApiResponse}
     * @return Single List of instance of {@link VenueSnapShot}
     */
    private Single<List<VenueSnapShot>> onVenuesResponse(VenueApiResponse response) {
        return Single.just(response.getResponse().getVenueSnapShots());
    }

    /**
     * Method converts {@link CategoriesApiResponse} into a list of {@link Category}
     *
     * @param response instance of {@link CategoriesApiResponse}
     * @return Single instance List of {@link Category}
     */
    private Single<List<Category>> onDataFetched(CategoriesApiResponse response) {
        if (response.getCategoriesApiData().getCategories() == null
                || response.getCategoriesApiData().getCategories().isEmpty()) {
            return Single.just(response.getCategoriesApiData().getCategories());
        }
        return Single.just(response.getCategoriesApiData().getCategories().stream()
                .flatMap(this::flattenList)
                .collect(Collectors.toList()));

    }

    /**
     * Method to flatten a nested List of {@link Category}
     *
     * @param category single {@link Category} element
     * @return Stream of {@link Category}
     */
    private Stream<Category> flattenList(Category category) {
        if (category.getCategories() == null || category.getCategories().isEmpty()) {
            return Stream.of(category);
        }
        return Stream.concat(Stream.of(category),
                category.getCategories().stream().flatMap(this::flattenList));
    }

    private Single<List<String>> getStringList(CategoriesApiResponse response) {
        if (response.getCategoriesApiData().getCategories() == null
                || response.getCategoriesApiData().getCategories().isEmpty()) {
            return Single.just(new ArrayList<>());
        }
        return Single.just(response.getCategoriesApiData().getCategories().stream()
                .flatMap(this::convertIntoString)
                .collect(Collectors.toList()));
    }

    private Stream<String> convertIntoString(Category category) {
        return Stream.concat(Stream.of(category.toString()),
                category.getCategories().stream().flatMap(this::convertIntoString));
    }


}
