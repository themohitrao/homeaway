package com.themohitrao.sdk.datasource;

import com.themohitrao.common.models.catagory.Category;
import com.themohitrao.common.models.venue.Venue;
import com.themohitrao.common.models.venue.VenueSnapShot;
import com.themohitrao.sdk.network.ModuleNetworkConnection;
import com.themohitrao.sdk.scopes.NetworkScope;

import java.util.List;

import javax.inject.Named;

import dagger.Component;
import io.reactivex.Single;

import static com.themohitrao.sdk.datasource.ModuleDataSource.NAMED_QUALIFIER_CATEGORIES;
import static com.themohitrao.sdk.datasource.ModuleDataSource.NAMED_QUALIFIER_SEARCH_LIST;
import static com.themohitrao.sdk.datasource.ModuleDataSource.NAMED_QUALIFIER_VENUES_BY_CATEGORY;
import static com.themohitrao.sdk.datasource.ModuleDataSource.NAMED_QUALIFIER_VENUES_BY_QUERY;
import static com.themohitrao.sdk.datasource.ModuleDataSource.NAMED_QUALIFIER_VENUE_DETAIL;

@NetworkScope
@Component(modules = {ModuleDataSource.class, ModuleNetworkConnection.class})
public interface ComponentDataSource {

    @Named(NAMED_QUALIFIER_CATEGORIES)
    Single<List<Category>> getCategories();

    @Named(NAMED_QUALIFIER_VENUES_BY_CATEGORY)
    Single<List<VenueSnapShot>> getVenuesByCategory();

    @Named(NAMED_QUALIFIER_VENUES_BY_QUERY)
    Single<List<VenueSnapShot>> getVenuesByQuery();

    @Named(NAMED_QUALIFIER_SEARCH_LIST)
    Single<List<String>> getSearchList();

    @Named(NAMED_QUALIFIER_VENUE_DETAIL)
    Single<Venue> getVenuesDetail();


}
